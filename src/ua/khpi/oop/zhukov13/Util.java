package ua.khpi.oop.zhukov13;

import ua.khpi.oop.zhukov07.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Допоміжні методи
 */
public class Util {
    /**
     * Очищає вікно консолі
     */
    public static void clearConsole() {
        System.out.print("\u001b[2J\u001b[H");
        System.out.flush();
    }

    /**
     * Створює випадковий рядок
     * @param len довжина рядка
     * @return <code>String</code> створений рядок
     */
    public static String randomString(int len) {
        StringBuilder str = new StringBuilder();
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        Random r = new Random();

        for (int i  = 0; i < len; i++) {
            str.append(chars[r.nextInt(chars.length)]);
        }

        return str.toString();
    }

    /**
     * Створює випадкову дату (рядок) в форматі dd-MM-yy
     * Мінімальний рік 2000, максимальний 2021
     * @return <code>String</code> створена дата (рядок)
     */
    public static String randomDateString() {
        Random r = new Random();

        return (1 + r.nextInt(30)) +
                "-" +
                (1 + r.nextInt(12)) +
                "-" +
                (2000 + r.nextInt(21));
    }

    /**
     * Створює випадковий десятковий дріб у
     * @param max максимальне значення від 1 до <code>max</code>
     * @return <code>double</code> створене число
     */
    public static double randomDouble(double max) {
        return new Random().nextDouble() * max;
    }

    /**
     * Створює випадкове ціле число в заданому діапазоні
     * @param max number bounds from 1 to max
     * @return <code>int</code> generated integer
     */
    public static int randomInt(int max) {
        return new Random().nextInt(max) + 1;
    }

    /**
     * Аналог метода <code>getch()</code> з мови C
     * Дозволяє розпізнати клавішу яка була натиснута в консолі
     * @return <code>int</code> код клавіші, яка була натиснута
     */
    public static int getCh() {
        final JFrame frame = new JFrame();
        final int[] key = new int[1];
        synchronized (frame) {
            frame.setUndecorated(true);
            frame.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
            frame.setType(Window.Type.UTILITY);
            frame.addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(KeyEvent e) {
                    synchronized (frame) {
                        frame.setVisible(false);
                        frame.dispose();
                        frame.notify();
                        key[0] = e.getKeyCode();
                    }
                }
                @Override
                public void keyReleased(KeyEvent e) {
                }
                @Override
                public void keyTyped(KeyEvent e) {
                }
            });
            frame.setVisible(true);
            try {
                frame.wait();
            } catch (InterruptedException e1) {
            }
        }

        return key[0];
    }

    /**
     * Створює об'єкт класу Product з випадковими даними
     * @return <code>Product</code> створений об'єкт
     */
    public static Product createRandomProduct() {
        return new Product(
                Util.randomString(15),
                Util.randomString(5),
                Util.randomInt(10000),
                Util.randomDouble(500),
                Util.randomDateString(),
                new HashMap<>()
        );
    }

    public static Date strToDate(String strDate) {
        try {
            return new SimpleDateFormat("dd-MM-yy").parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new Date();
    }
}
