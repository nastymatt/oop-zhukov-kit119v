package ua.khpi.oop.zhukov06;
import ua.khpi.oop.zhukov08.Main;

import java.io.*;
import java.nio.file.Paths;
import java.util.Iterator;

/**
 * Клас контейнер
 */
public class IList {
    /**
     * Масив даних для контейнера
     */
    private String[] arr;
    public IList() {
        arr = new String[]{};
    }
    /**
     * Повертає вміст контейнера у вигляді рядка
     * @return рядок даних контейра розділених пробілом
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (String s : arr)
            builder.append(s).append(" ");
        return builder.toString();
    }
    /**
     * Додає вказанний елемент до кінця контенеру
     * @param str рядок який буде додано
     */
    public void add(String str) {
        int i;
        String[] oldArr = arr;
        arr = new String[oldArr.length + 1];
        for (i = 0; i < oldArr.length; i++)
            arr[i] = oldArr[i];
        arr[i] = str;
    }

    /**
     * Видаляє всі елементи з контейнеру
     */
    public void clear() {
        arr = new String[]{};
    }
    /**
     * Видаляє перший випадок вказанного елемента з контейнера
     * @param str рядок для видалення
     * @return true якщо було знайдено випадок
     */
    public boolean remove(String str) {
        String[] oldArr = arr;
        if (!contains(str)) return false;
        arr = new String[oldArr.length - 1];
        for (int i = 0, j = 0; i < oldArr.length; i++)
            if (!oldArr[i].contains(str))
                arr[j++] = oldArr[i];
        return true;
    }
    /**
     * Повертає масив, що містить всі елементи  контейнері
     * @return масив елементів
     */
    public Object[] toArray() {
        return arr; }
    /**
     * Отримати розмір контейнеру
     * @return розмір контейнеру
     */
    public int size() {
        return arr.length;
    }
    /**
     * Знаходження вказанного елементу
     * @param str шуканий рядок
     * @return true якщо рядок було знайдено
     */
    public boolean contains(String str) {
        for (String s : arr)
            if (s.equals(str)) return true;

        return false;
    }
    /**
     * Порівняння контейнерів
     * @param list контейнер для порівняння
     * @return true якщо контейнер містить всі елементи з
    зазаначеного у параметрах
     */
    public boolean containsAll(IList list) {
        int i, j;
        for (i = 0; i < list.size(); i++) {
            for (j = 0; j < arr.length; j++)
                if (list.arr[i].equals(arr[j])) break;
            if (j == arr.length) return false;
        }
        return true;
    }
    /**
     * Класс ітератор
     * @return ітератор для контейнера елементів
     */
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private int currentIndex = 0;
            /**
             * Перевіряє існування наступного елемента контейнеру
             * @return true якщо елемент існує
             */
            @Override
            public boolean hasNext() {
                return currentIndex < size() && arr[currentIndex] != null;
            }
            /**
             * Повертає наступний елемент контейнеру
             * @return елемент контейнеру
             */
            @Override
            public String next() {
                return arr[currentIndex++];
            }
            /**
             * Видаляє поточний елемент с контейнера
             */

            @Override
            public void remove() {
                IList.this.remove(arr[currentIndex]);
            }
        };
    }

    /**
     * Зчитування контейнера з файлу (десеріалізація)
     * @param filePath шлях до файлу
     */
    public void read(String filePath) {
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath.replace("file:", ""));
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            arr = (String[]) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("File not found " + filePath);
        }
    }

    /**
     * Запис контейнера у файл (серіалізація)
     * @param filePath шлях до файлу
     */
    public void write(String filePath) {
        try {
            FileOutputStream outputStream = new FileOutputStream(filePath.replace("file:", ""));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(arr);
            objectOutputStream.close();
        } catch (IOException e) {
            System.out.println("File not found " + filePath);
        }
    }

    /**
     * Пошук елементів в контецнері
     * @param str рядок для пошуку
     * @return масив рядків - результати пошуку
     */
    public String[] find(String str) {
        String[] results = new String[]{};

        if (str == null) return results;

        for (String s : arr) {
            if (s.contains(str)) {
                String[] _re = results;
                results = new String[_re.length + 1];
                int i;

                for (i = 0; i < _re.length; i++)
                    results[i] = _re[i];

                results[i] = s;
            }
        }

        return results;
    }

    /**
     * Сортування масиву за вказаним параметром
     * @param sb параметр сортування
     */
    public void sort(int sb) {
        int i, j;
        String tmp;

        switch (sb) {
            case 1:
                for (i = 0; i < arr.length; i++) {
                    for (j = i + 1; j < arr.length; j++) {
                        if (arr[i].length() > arr[j].length()) {
                            tmp = arr[i];
                            arr[i] = arr[j];
                            arr[j] = tmp;
                        }
                    }
                }
                break;
            case 2:
                for (i = 0; i < arr.length; i++) {
                    for (j = i + 1; j < arr.length; j++) {
                        if (arr[i].compareTo(arr[j]) > 0) {
                            tmp = arr[i];
                            arr[i] = arr[j];
                            arr[j] = tmp;
                        }
                    }
                }
                break;
        }
    }
}