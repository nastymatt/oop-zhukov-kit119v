package ua.khpi.oop.zhukov09;

/**
 * Параметризований вузол списку
 * @param <T> тип елемента вузла
 */
public class Node<T> {
    private T value;
    private Node<T> next;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }
}
