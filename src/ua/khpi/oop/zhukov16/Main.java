package ua.khpi.oop.zhukov16;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ui/Main.fxml"));

        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.setTitle("Лабораторна робота №16");
        stage.show();
    }
}
