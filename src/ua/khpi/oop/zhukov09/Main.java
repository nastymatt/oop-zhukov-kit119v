package ua.khpi.oop.zhukov09;

import ua.khpi.oop.zhukov07.Product;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        NodeList<Product> nodeList = new NodeList<>();
        Map<String, String> d1 = new HashMap<>();
        String txtPath = "D:\\Study\\OOP\\oop-khpi\\src\\ua\\khpi\\oop\\zhukov09\\nodelist.txt";
        String xmlPath = "D:\\Study\\OOP\\oop-khpi\\src\\ua\\khpi\\oop\\zhukov09\\nodelist.xml";

        d1.put("color", "red");

        nodeList.add(new Product("apple", "kg", 231, 22.5, "22-10-2020", d1));

        d1.clear();
        d1.put("size", "small");

        nodeList.add(new Product("pineapple", "kg", 533, 67.8, "22-11-2020", d1));
        nodeList.write(txtPath);

        System.out.println(nodeList.toString());

        nodeList.clear();
        nodeList.read(txtPath);

        d1.clear();
        d1.put("color", "yellow");

        nodeList.add(new Product("cherry", "kg", 1047, 53.4, "27-11-2020", d1));
        System.out.println(nodeList.toString());

        nodeList.writeXML(xmlPath);
        nodeList.clear();

        System.out.println(nodeList.toString());
        nodeList.readXML(xmlPath);
    }
}
