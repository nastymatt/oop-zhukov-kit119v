package ua.khpi.oop.zhukov09;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

public class NodeList<T> implements Iterable<T> {
    protected Node<T> head;
    protected Node<T> tail;

    /**
     * Створює екземпляр контейнера з елементів масиву
     * @param arr масив елементів з якими буде створено контейнер
     * @param <U> тип елементів контейнера
     * @return <code>NodeList</code> контейнер
     */
    public static <U> NodeList<U> fromArray(U[] arr) {
        NodeList<U> list = new NodeList<>();

        for (U e : arr) {
            list.add(e);
        }

        return list;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "[]";
        }

        Node<T> _head = head;
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("[");

        while (_head != null) {
            stringBuilder
                    .append(_head.getValue().toString())
                    .append(",\n");

            _head = _head.getNext();
        }

        stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");

        return stringBuilder.toString();
    }

    /**
     * Довжна списку
     * @return 0 якщо список пустий
     */
    public int getSize() {
        Node<T> _head = head;
        int size = 0;

        while (_head != null) {
            size += 1;
            _head = _head.getNext();
        }

        return size;
    }

    /**
     * Додати новий елемент до списку
     * @param value значення елемента
     */
    public void add(T value) {
        Node<T> node = new Node<>();

        node.setValue(value);

        if (head == null) {
            head = node;
        } else {
            tail.setNext(node);
        }
        tail = node;
    }

    /**
     * Отримати елемент за його індексом
     * @param index індекс шуканого елемента
     * @return <code>T</code>, якщо елемент було знайдено і <code>null</code>, якщо не знайдено
     */
    public T getValue(int index) {
        Node<T> node = findByIndex(index);

        if (node == null) {
            return null;
        }

        return node.getValue();
    }

    /**
     * Видалити елемент із списку за його індексом
     * @param index індекс видаляємого елемента
     * @return <code>true</code> якщо елемент було успішно видалено
     */
    public boolean remove(int index) {
        if (index < 0 || index > getSize() - 1) {
            return false;
        }

        if (index == 0) {
            head = head.getNext();
        } else {
            Node<T> nodeBeforeIndex = findNodeBeforeIndex(index);
            Node<T> nodeByIndex = findByIndex(index);

            if (nodeBeforeIndex == null || nodeByIndex == null) {
                return false;
            }

            nodeBeforeIndex.setNext(nodeByIndex.getNext());

            return true;
        }

        return false;
    }

    /**
     * Перетворення списку в масив
     * @return масив елементів списку
     */
    public T[] toArray() {
        if (head == null) {
            return (T[]) Array.newInstance(head.getValue().getClass(), 0);
        }

        T[] arr = (T[]) Array.newInstance(head.getValue().getClass(), getSize());
        Node<T> _head = head;
        int i = 0;

        while (_head != null) {
            arr[i++] = _head.getValue();

            _head = _head.getNext();
        }

        return arr;
    }

    /**
     * Перевірка на наявність елементу
     * @param value шуканий елемент
     * @return <code>true</code> якщо елемент було знайдено
     */
    public boolean contains(T value) {
        Node<T> _head = head;

        while (_head != null) {
            if (_head.getValue().equals(value)) {
                return true;
            }

            _head = _head.getNext();
        }

        return false;
    }

    public void clear() {
        head = null;
        tail = null;
    }

    /**
     * Клас ітератор
     * @return ітератор для використання списку в циклі foreach в якості джерела даних
     */
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Node<T> _head = head;

            @Override
            public boolean hasNext() {
               return _head != null;
            }

            @Override
            public T next() {
                T val = _head.getValue();
                _head = _head.getNext();

                return val;
            }
        };
    }

    /**
     * Запис спсику у файл не використовуючи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void write(String filePath) {
        try {
            Node<T> _head = head;
            StringBuilder stringBuilder = new StringBuilder();

            if (head == null) {
                return;
            }

            stringBuilder.append(head.getValue().getClass().getName());

            while (_head != null) {
                stringBuilder
                        .append("\n")
                        .append(_head.getValue().toString());
                _head = _head.getNext();
            }

            Files.writeString(Paths.get(filePath), stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Зчитування списку з файлу не використовуючи протокол серіалізацї
     * @param filePath шлях до файлу
     */
    public void read(String filePath) {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            Class<?> act = null;

            clear();

            for (Iterator<String> it = stream.iterator(); it.hasNext();) {
                String line = it.next();

                if (act == null) {
                    try {
                        act = Class.forName(line);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        add((T) act.cast(act.getDeclaredConstructor(String.class).newInstance(line)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Запис списку у файл використовуючи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void writeXML(String filePath) {
        try {
            XMLEncoder encoder = new XMLEncoder(new FileOutputStream(filePath));
            encoder.writeObject(toArray());
            encoder.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Зчитування списку з файлу використувуючи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void readXML(String filePath) {
        try {
            XMLDecoder decoder = new XMLDecoder(new FileInputStream(filePath));
            T[] arr = (T[]) decoder.readObject();

            clear();

            for (T val : arr) {
                add(val);
            }

            decoder.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Пошук вузла після вказанного індекса
     * @param index індекс для пошуку
     * @return вузол списку або <code>null</code>
     */
    private Node<T> findNodeBeforeIndex(int index) {
        if (index <= 0 || index > getSize() - 1) {
            return null;
        }

        int i = 0;
        Node<T> _head = head;

        while (_head.getNext() != null) {
            if (i == index - 1) {
                return  _head;
            }

            i++;
            _head = _head.getNext();
        }

        return null;
    }

    /**
     * Пошук вузла за вказаним індексом
     * @param index індекс для пошуку
     * @return вузол списку або <code>null</code>
     */
    private Node<T> findByIndex(int index) {
        if (index < 0 || index > getSize() - 1) {
            return null;
        }

        if (head == null) {
            return null;
        }

        if (index == 0) {
            return head;
        }

        Node<T> _head = head;
        int i = 0;

        while (_head.getNext() != null) {
            _head = _head.getNext();
            i++;

            if (i == index) {
                return _head;
            }
        }

        return null;
    }
}
