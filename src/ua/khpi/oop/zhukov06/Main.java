package ua.khpi.oop.zhukov06;

import ua.khpi.oop.zhukov04.Helper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    /**
     * Очекування клавіші Enter
     */
    public static void waitForEnter() {
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Очищення вікна консолі
     */
    public static void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    /**
     * Меню програми
     */
    public static void appMenu() {
        IList list = new IList();
        Scanner input = new Scanner(System.in);
        final Path clPath = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().toString());
        String str;
        int sb, option = 0;

        while (true) {
            clearConsole();
            System.out.println(
                    "\t0 - Вихід\n"
                            + "\t1 - Додати до контейнеру\n"
                            + "\t2 - Видалити з контейнеру\n"
                            + "\t3 - Зчитати контейнер з файлу\n"
                            + "\t4 - Записати контейнер у файл\n"
                            + "\t5 - Вивести контейнер\n"
                            + "\t6 - Сортувати контейнер за [1 - довжиною, 2 - алфавітом]\n"
                            + "\t7 - Пошук в контейнері\n"
                            + "\t8 - Обробка елементів відкомпільованим классом\n"
                            + "\t9 - Обрабка елементів власним классом\n"
            );

            option = input.nextInt();

            switch (option) {
                case 0: return;
                case 1:
                    input.nextLine();
                    str = input.nextLine();
                    list.add(str);
                    break;
                case 2:
                    input.nextLine();
                    str = input.nextLine();
                    list.remove(str);
                    break;
                case 3:
                    input.nextLine();
                    str = input.nextLine();
                    list.read(clPath.resolve(str).toString());
                    waitForEnter();
                    break;
                case 4:
                    input.nextLine();
                    str = input.nextLine();
                    list.write(clPath.resolve(str).toString());
                    waitForEnter();
                    break;
                case 5:
                    System.out.println(list.toString());
                    waitForEnter();
                    break;
                case 6:
                    input.nextLine();
                    sb = input.nextInt();
                    list.sort(sb);
                    break;
                case 7:
                    input.nextLine();
                    str = input.nextLine();
                    System.out.println("Результати пошуку ->");
                    for (String s : list.find(str))
                        System.out.println(s);
                    waitForEnter();
                    break;
                case 8:
//                    ua.khpi.oop.abdulaev03.Helper.stringsInfo(list.toString());
                    waitForEnter();
                    break;
                case 9:
                    input.nextLine();
                    System.out.println("Введіть слово для заміни:");
                    str = input.nextLine();
                    System.out.println("Введіть довжну слів для заміни:");
                    sb = input.nextInt();
                    System.out.println(Helper.replaceByWordLength(list.toString(), str, sb));
                    waitForEnter();
            }
        }
    }

    public static void main(String[] args) {
        clearConsole();
        appMenu();
    }
}
