package ua.khpi.oop.zhukov01;

public class Main {

    static int evenCount(long n) {
        int count = 0;

        while (n > 0) {
            if ((n % 10) % 2 == 0) count++;
            n /= 10;
        }

        return count;
    }

    static int binaryOneCount(long n) {
        int count = 0;

        for (; n > 0; count++) {
            n &= n - 1;
        }

        return count;
    }

    static int oddCount(long n) {
        int count = 0;

        while (n > 0) {
            if ((n % 10) % 2 != 0) count++;
            n /= 10;
        }

        return count;
    }

    public static void main(String[] args) {
        int recordBook = 0x19b61; //105313
        long phoneNumber = 380689215359L;
        int lastTwo = 0b111011; //59
        int lastFour = 012357; //5359
        int numberJournal = ((11 - 1) % 26) + 1;
        char letterByNumber = 'K'; //10th letter of abc
        long[] allEven = {recordBook, phoneNumber, lastTwo, lastFour, numberJournal};
        long[] allOdd = {recordBook, phoneNumber, lastTwo, lastFour, numberJournal};
        long[] allBinaryOne = {recordBook, phoneNumber, lastTwo, lastFour, numberJournal};

        for (int i = 0; i < allEven.length; i++) {
            allEven[i] = evenCount(allEven[i]);
        }

        for (int i = 0; i < allOdd.length; i++) {
            allOdd[i] = oddCount(allOdd[i]);
        }

        for (int i = 0; i < allBinaryOne.length; i++) {
            allBinaryOne[i] = binaryOneCount(allBinaryOne[i]);
        }

    }

}