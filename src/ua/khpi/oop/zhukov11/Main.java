package ua.khpi.oop.zhukov11;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov10.ShopSort;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void modeAuto() {
        Shop11 shop11 = new Shop11();

        shop11.readXML("D:\\Study\\OOP\\oop-khpi\\out\\production\\oop-khpi\\ua\\khpi\\oop\\zhukov09\\nodelist.xml");

        System.out.printf("Before sort:\n%s\n\n", shop11.toString());

        shop11.sort(ShopSort.BY_AMOUNT, true);

        System.out.printf("After sort by amount descending:\n%s\n", shop11.toString());
    }

    public static void modeMenu() {
        Shop11 shop11 = new Shop11();

        Scanner scanner = new Scanner(System.in);
        String filePath;
        int mainMenuOption;

        while (true) {
            clearConsole();

            System.out.println(
                    "1. Add element\n" +
                            "2. Remove element\n" +
                            "3. Contains element\n" +
                            "4. Read from file\n" +
                            "5. Write to file\n" +
                            "6. Print\n" +
                            "7. Clear\n" +
                            "8. Sort\n" +
                            "0. Exit"
            );

            mainMenuOption = scanner.nextInt();

            switch (mainMenuOption) {
                case 0:
                    return;
                case 1:
                    try {
                        shop11.add(inputProduct(scanner));
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                        scanner.nextLine();
                    }
                    break;
                case 2:
                    int rmIndex = scanner.nextInt();

                    clearConsole();

                    if (shop11.remove(rmIndex)) {
                        System.out.printf("Successfully removed item with index %d", rmIndex);
                    } else {
                        System.out.printf("Cannot find item with index %d", rmIndex);
                    }
                    scanner.nextLine();

                    break;
                case 3:
                    try {
                        if (shop11.contains(inputProduct(scanner))) {
                            System.out.println("True");
                        } else {
                            System.out.println("False");
                        }
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }

                    scanner.nextLine();
                    break;
                case 4:
                    clearConsole();

                    System.out.println(
                            "1. Read text\n" +
                                    "2. Read XML\n" +
                                    "0. Back");

                    mainMenuOption = scanner.nextInt();

                    switch (mainMenuOption) {
                        case 1:
                            System.out.print("File path: ");
                            scanner.nextLine();

                            filePath = scanner.nextLine();

                            shop11.read(filePath);
                            break;
                        case 2:
                            System.out.print("File path: ");
                            scanner.nextLine();

                            filePath = scanner.nextLine();

                            shop11.readXML(filePath);
                            break;
                    }
                    break;
                case 5:
                    clearConsole();

                    System.out.println(
                            "1. Write text\n" +
                                    "2. Write XML\n" +
                                    "0. Back");

                    mainMenuOption = scanner.nextInt();

                    switch (mainMenuOption) {
                        case 1:
                            System.out.print("File path: ");
                            scanner.nextLine();

                            filePath = scanner.nextLine();

                            shop11.write(filePath);
                            break;
                        case 2:
                            System.out.print("File path: ");
                            scanner.nextLine();

                            filePath = scanner.nextLine();

                            shop11.writeXML(filePath);
                            break;
                    }
                    break;
                case 6:
                    System.out.println(shop11.toString());
                    scanner.nextLine();
                    scanner.nextLine();
                    break;
                case 7:
                    shop11.clear();
                    break;
                case 8:
                    clearConsole();

                    System.out.println("1. By name ascending\n" +
                            "2. By name descending\n" +
                            "3. By amount ascending\n" +
                            "4. By amount descending\n" +
                            "5. By date ascending\n" +
                            "6. By date descending\n");

                    mainMenuOption = scanner.nextInt();

                    switch (mainMenuOption) {
                        case 1: shop11.sort(ShopSort.BY_NAME, false);
                        case 2: shop11.sort(ShopSort.BY_NAME, true);
                        case 3: shop11.sort(ShopSort.BY_AMOUNT, false);
                        case 4: shop11.sort(ShopSort.BY_AMOUNT, true);
                        case 5: shop11.sort(ShopSort.BY_DATE, false);
                        case 6: shop11.sort(ShopSort.BY_DATE, true);
                    }
            }
        }
    }

    public static Product inputProduct(Scanner scanner) {
        String pName, pUnit, pReceiptDate, pDescriptionStr, pAmount, pPrice;
        Map<String, String> pDescription = new HashMap<>();

        clearConsole();
        scanner.nextLine();

        System.out.print("Product name: ");
        pName = scanner.nextLine();

        System.out.print("Product unit: ");
        pUnit = scanner.nextLine();

        System.out.print("Product receipt date: ");
        pReceiptDate = scanner.nextLine();

        System.out.print("Product price: ");
        pPrice = scanner.nextLine();

        System.out.print("Product amount: ");
        pAmount = scanner.nextLine();

        System.out.print("Product description: ");
        pDescriptionStr = scanner.nextLine();

        if (!pName.matches("^\\w+$") ||
            !pUnit.matches("^\\w+$") ||
            !pReceiptDate.matches("^\\d+-\\d+-\\d+$") ||
            !pPrice.matches("^\\d+(\\.*\\d+)?$") ||
            !pAmount.matches("^\\d+$") ||
            !pDescriptionStr.matches("^((\\w+=\\w+)?(, )?)+$"))
        {
            throw new RuntimeException("Invalid input data!");
        }

        String[] pairs = pDescriptionStr.split(", ");

        if (!pDescriptionStr.equals("")) {
            for (String kv : pairs) {
                String[] kvArr = kv.split("=");
                pDescription.put(kvArr[0], kvArr[1]);
            }
        }

        return new Product(
                pName,
                pUnit,
                Integer.parseInt(pAmount),
                Double.parseDouble(pPrice),
                pReceiptDate,
                pDescription
        );
    }

    public static void main(String[] args) {
        clearConsole();

        if (args.length > 0 && args[0].equals("-auto")) {
            modeAuto();
        } else {
            modeMenu();
        }
    }
}
