package ua.khpi.oop.zhukov11;

import ua.khpi.oop.zhukov07.Product;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 * Прикладна область "Shop"
 */
public class Shop11 extends ua.khpi.oop.zhukov10.Shop {
    /**
     * Зчитування даних про продукти з файлу, з валідацією регулярними виразами
     * @param filePath шлях до файлу
     */
    @Override
    public void read(String filePath) {
        String regex = "Product\\{name=.+, unit=.+, amount=\\d+, price=\\d+(\\.*\\d+)?, receiptDate=\\d+-\\d+-\\d+, description=\\{((\\w+=[\\w\\-.]+)?(, )?)+}}";

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            clear();

            for (Iterator<String> it = stream.iterator(); it.hasNext();) {
                String line = it.next();

                if (line.matches(regex)) {
                    add(new Product(line));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
