package ua.khpi.oop.zhukov15;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov13.Menu;
import ua.khpi.oop.zhukov13.Util;

import java.io.IOException;
import java.util.*;

public class Main {
    static Shop15 shop = new Shop15();

    public static void main(String[] args) {
        List<String> argsList = Arrays.asList(args);

        if (argsList.size() > 0 && argsList.contains("-auto")) {
            modeAuto();
            return;
        }

        modeMenu();
    }

    public static void modeMenu() {
        Menu mainMenu = new Menu();
        Menu readFileMenu = new Menu();
        Menu writeFileMenu = new Menu();
        Menu sortMenu = new Menu();
        Scanner scanner = new Scanner(System.in);

        readFileMenu
                .on("Відновити з текстового файлу", () -> {
                    System.out.print("Шлях до файлу: ");
                    String filePath = scanner.nextLine();

                    try {
                        shop.loadFromFile(filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Відновлення успішно");
                    scanner.nextLine();
                })
                .on("Відновити з XML файлу", () -> {
                    System.out.print("Шлях до файлу: ");
                    String filePath = scanner.nextLine();

                    try {
                        shop.loadFromXMLFile(filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Відновлення успішно");
                    scanner.nextLine();
                });

        writeFileMenu
                .on("Зберегти до текстового файлу", () -> {
                    System.out.print("Шлях до файлу: ");
                    String filePath = scanner.nextLine();

                    try {
                        shop.saveToFile(filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Збереження успішно");
                    scanner.nextLine();
                })
                .on("Зберегти до XML файлу", () -> {
                    System.out.print("Шлях до файлу: ");
                    String filePath = scanner.nextLine();

                    try {
                        shop.saveToXMLFile(filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Збереження успішно");
                    scanner.nextLine();
                });

        sortMenu
                .on("[ЗРОСТАННЯ] По назві", () -> {
                    shop.sort(Comparator.comparing(Product::getName));
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                })
                .on("[ЗРОСТАННЯ] По кількості товару", () -> {
                    shop.sort(Comparator.comparing(Product::getAmount));
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                })
                .on("[ЗРОСТАННЯ] По ціні товару", () -> {
                    shop.sort(Comparator.comparing(Product::getPrice));
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                })
                .on("[СПАДАННЯ]  По назві", () -> {
                    shop.sort(Comparator.comparing(Product::getName).reversed());
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                })
                .on("[СПАДАННЯ]  По кількості товару", () -> {
                    shop.sort(Comparator.comparing(Product::getAmount).reversed());
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                })
                .on("[СПАДАННЯ]  По ціні товару", () -> {
                    shop.sort(Comparator.comparing(Product::getPrice).reversed());
                    System.out.println("Сортування успішно");
                    scanner.nextLine();
                });


        mainMenu
                .on("Вивести контейнер", () -> {
                    System.out.println(shop.toString());
                    scanner.nextLine();
                })
                .on("Додати елемент", () -> {
                    Util.clearConsole();

                    try {
                        shop.add(ua.khpi.oop.zhukov12.Main.inputProduct(scanner));
                        System.out.println("Елемент успішно додано");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    scanner.nextLine();
                })
                .on("Видалити елемент", () -> {
                    System.out.print("Індекс елемента який буде видалено: ");
                    String indexStr = scanner.nextLine();

                    try {
                        shop.remove(Integer.parseInt(indexStr));
                        System.out.println("Видалення успішно");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    scanner.nextLine();
                })
                .on("Пошук елементів", () -> {
                    System.out.print("Правило пошуку: ");
                    String searchQ = scanner.nextLine();
                    List<Object> searchResults = Arrays.asList(shop.find(product -> product.toString().contains(searchQ)));

                    System.out.println("\nРезультати пошуку: " + searchResults.size());
                    System.out.println(searchResults);

                    scanner.nextLine();
                })
                .on("Сортування елементів", sortMenu::launch)
                .on("Відновити з файлу", readFileMenu::launch)
                .on("Зберегти у файл", writeFileMenu::launch)
                .on("Заповнити видадковими значеннями", () -> {
                    System.out.print("Кількість випадкових елементів: ");
                    String countStr = scanner.nextLine();

                    try {
                        shop.fill(Integer.parseInt(countStr));
                        System.out.println("Заповнення успішно");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    scanner.nextLine();
                })
                .on("Вихід", () -> System.exit(0));

        mainMenu.launch();
    }

    public static void modeAuto() {
        final String filePath = "D:\\Study\\OOP\\oop-khpi\\src\\ua\\khpi\\oop\\zhukov15\\list.txt";

        shop.fill(10);

        System.out.println("Пошук товарів з ціною більшою за 400");
        System.out.println(Arrays.toString(shop.find(product -> product.getPrice() > 400)));

        System.out.println("Збережння контейнера у файл без серіалізації");
        try {
            shop.saveToFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Очищення контейнера");
        shop.clear();

        System.out.println("Відновлення контейнера з файлу");
        try {
            shop.loadFromFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
