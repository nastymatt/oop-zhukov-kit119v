package ua.khpi.oop.zhukov16.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import ua.khpi.oop.zhukov07.Product;
import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.ResourceBundle;

public class view_SortController implements Initializable {
    @FXML
    public TextArea areaLogcat;
    @FXML
    private CheckBox isReverseOrderCheckbox;
    @FXML
    private ChoiceBox sortBySelect;
    private HashMap<String, Comparator<Product>> comparatorHashMap;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        comparatorHashMap = new HashMap<>() {{
            put("За назвою", Comparator.comparing(Product::getName));
            put("За ціною", Comparator.comparing(Product::getPrice));
            put("За кількістю", Comparator.comparing(Product::getAmount));
            put("За датою отримання", (c, n) -> {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    return dateFormat.parse(c.getReceiptDate()).compareTo(dateFormat.parse(n.getReceiptDate()));
                } catch (Exception ignored) { }
                return -1;
            });
        }};
    }

    public void onSortButtonClick(MouseEvent event) {
        Comparator<Product> comparator = comparatorHashMap.get(sortBySelect.getValue().toString());

        if (isReverseOrderCheckbox.isSelected()) {
            shop.sort(comparator.reversed());
        } else {
            shop.sort(comparator);
        }

        areaLogcat.appendText("Сортування успішно");
    }
}
