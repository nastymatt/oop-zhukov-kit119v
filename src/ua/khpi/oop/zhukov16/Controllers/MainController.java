package ua.khpi.oop.zhukov16.Controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private TreeView<TreeItem<String>> globalNav;
    @FXML
    private AnchorPane framesContainer;
    private final HashMap<String, URL> menuOptionsPaths;

    public MainController() {
        menuOptionsPaths = new HashMap<>() {{
            put("Головне Меню", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_MainMenu.fxml"));
            put("Показати контейнер", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_ShowContainer.fxml"));
            put("Додати елемент", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_AddElement.fxml"));
            put("Видалити елемент", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_RemoveElement.fxml"));
            put("Пошук елементів", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_SearchElement.fxml"));
            put("Сортування", getClass().getResource("/ua/khpi/oop/zhukov16/ui/view_Sort.fxml"));
        }};
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initializeGlobalNav();
    }

    private void initializeGlobalNav() {
        TreeItem treeRoot = new TreeItem("Головне Меню");
        ArrayList treeItems = new ArrayList<>();
        MultipleSelectionModel msm = globalNav.getSelectionModel();

        treeItems.add(new TreeItem("Показати контейнер"));
        treeItems.add(new TreeItem("Додати елемент"));
        treeItems.add(new TreeItem("Видалити елемент"));
        treeItems.add(new TreeItem("Пошук елементів"));
        treeItems.add(new TreeItem("Сортування"));

        treeRoot.setExpanded(true);

        treeRoot.getChildren().addAll(treeItems);
        globalNav.setRoot(treeRoot);

        msm.select(0);
        handleGlobalNav("Головне Меню");
    }

    @FXML
    private void onTreeViewClicked(MouseEvent event) {
        Node node = event.getPickResult().getIntersectedNode();

        if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
            handleGlobalNav((String) ((TreeItem)globalNav.getSelectionModel().getSelectedItem()).getValue());
        }
    }

    private boolean handleGlobalNav(String name) {
        if (!menuOptionsPaths.containsKey(name)) {
            return false;
        }

        loadFXMLIntoView(menuOptionsPaths.get(name));

        return true;
    }

    private void loadFXMLIntoView(URL url) {
        try {
            framesContainer.getChildren().setAll((AnchorPane) FXMLLoader.load(url));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File showFileDialog(String type, String filterTitle, String filter) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(filterTitle, filter);
        Window currentWindow = globalNav.getScene().getWindow();
        File file = null;

        fileChooser.getExtensionFilters().add(extFilter);

        switch (type) {
            case "OPEN":
                fileChooser.setTitle("Відновити з файлу...");
                file = fileChooser.showOpenDialog(currentWindow);
                break;
            case "SAVE":
                fileChooser.setTitle("Зберегти до файлу...");
                file = fileChooser.showSaveDialog(currentWindow);
                break;
        }

        return file;
    }

    private void showAlert(Alert.AlertType alertType, String header, String msg) {
        Alert alert = new Alert(alertType);

        alert.setTitle(header);
        alert.setContentText(msg);
        alert.show();
    }

    public void onMenuExitClick(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void onRestoreFromTextMenuClick(ActionEvent actionEvent) {
        File file = showFileDialog("OPEN", "TXT файли (*.txt)", "*.txt");

        try {
            shop.loadFromFile(file.getAbsolutePath());
            showAlert(Alert.AlertType.INFORMATION, "Відновлення", "Відновлення успішно");
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Помилка", e.toString());
        }
    }

    public void onRestoreFromXMLMenuClick(ActionEvent actionEvent) {
        File file = showFileDialog("OPEN", "XML файли (*.xml)", "*.xml");

        try {
            shop.loadFromXMLFile(file.getAbsolutePath());
            showAlert(Alert.AlertType.INFORMATION, "Відновлення", "Відновлення успішно");
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Помилка", e.toString());
        }
    }

    public void onSaveToTextClick(ActionEvent actionEvent) {
        File file = showFileDialog("SAVE", "TXT файли (*.txt)", "*.txt");

        try {
            shop.saveToFile(file.getAbsolutePath());
            showAlert(Alert.AlertType.INFORMATION, "Збереження", "Успішно збережено");
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Помилка", e.toString());
        }
    }

    public void onSaveToXMLClick(ActionEvent actionEvent) {
        File file = showFileDialog("SAVE", "XML файли (*.xml)", "*.xml");

        try {
            shop.saveToXMLFile(file.getAbsolutePath());
            showAlert(Alert.AlertType.INFORMATION, "Збереження", "Успішно збережено");
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Помилка", e.toString());
        }
    }

    public void onAboutMenuClick(ActionEvent actionEvent) {
        showAlert(Alert.AlertType.INFORMATION, "Про програму",
                "Лабораторна робота №16\n" +
                "студента групи КІТ-119в\n+" +
                "Жукова Дмитра");
    }
}
