package ua.khpi.oop.zhukov05;
import java.util.Iterator;
public class Main {
    public static void main(String[] args) {
        IList list = new IList();
        list.add("Start");
        list.add("Data1");
        list.add("Data2");
        list.add("End");
        System.out.printf("- IList.toString() -> \n\t%s\n", list.toString());
        Iterator<String> iterator = list.iterator();
        int iterIndex = 1;
        System.out.println("- IList.iterator() [while] ->");
        while (iterator.hasNext()) {
            String str = iterator.next();
            System.out.printf("\t%d) %s\n", iterIndex++, str);
        }
        iterIndex = 1;
        System.out.println("- IList.iterator() [for] ->");
        for (iterator = list.iterator(); iterator.hasNext();) {
            String str = iterator.next();
            System.out.printf("\t%d) %s\n", iterIndex++, str);
        }
        System.out.println("- IList.remove(\"End\") -> ");
        list.remove("End");
        printList(list);
        System.out.println("- IList.add(\"Data3\") ->");
        list.add("Data3");
        printList(list);
        iterIndex = 0;
        int indexToRemove = 2;
        System.out.printf("- IList.iterator().remove() [remove %dth element] ->\n", indexToRemove);
        for (iterator = list.iterator(); iterator.hasNext();
             iterIndex++, iterator.next())
            if (iterIndex == indexToRemove)
                iterator.remove();
        printList(list);
        String strToFind = "Data2";
        System.out.printf("- IList.contains(\"%s\") -> \n\t%b\n", strToFind, list.contains(strToFind));
        System.out.printf("- IList.size() -> \n\t%d\n", list.size());
        list.clear();
        System.out.printf("IList.clear() [then IList.size()] -> \n\t%d\n", list.size());
    }
    /**
     * Форматоване виведення контейнеру в консоль
     * @param list контейнер
     */
    public static void printList(IList list) {
        int i = 1;
        for (Iterator<String> it = list.iterator(); it.hasNext();)
            System.out.printf("\t%d) %s\n", i++, it.next());

    }
}

