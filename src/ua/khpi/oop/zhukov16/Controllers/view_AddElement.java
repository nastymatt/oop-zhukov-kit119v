package ua.khpi.oop.zhukov16.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ua.khpi.oop.zhukov07.Product;

import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class view_AddElement implements Initializable {
    @FXML
    public DatePicker productReceiptDateInput;
    @FXML
    public TextArea areaLogcat;
    @FXML
    private Button productAddButton;
    @FXML
    private Button productClearButton;
    @FXML
    private TextField productNameInput;
    @FXML
    private TextField productPriceInput;
    @FXML
    private TextField productAmountInput;
    @FXML
    private TextField productUnitInput;
    @FXML
    private TextField productDescriptionInput;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void onProductAddClick(ActionEvent actionEvent) {
        try {
            String pName = productNameInput.getText();
            String pPrice = productPriceInput.getText();
            String pAmount = productAmountInput.getText();
            String pUnit = productUnitInput.getText();
            String pDescriptionStr = productDescriptionInput.getText();
            String pReceiptDate = productReceiptDateInput.getValue().toString().replaceAll("\\.", "-");
            HashMap<String, String> pDescription = new HashMap<>();
            String[] pairs;

            if (!pName.matches("^\\w+$") ||
                    !pUnit.matches("^\\w+$") ||
                    !pReceiptDate.matches("^\\d+-\\d+-\\d+$") ||
                    !pPrice.matches("^\\d+(\\.*\\d+)?$") ||
                    !pAmount.matches("^\\d+$") ||
                    !pDescriptionStr.matches("^((\\w+=\\w+)?(, )?)+$")) {
                areaLogcat.appendText("Невірні вхідні дані, перевірте правильність вхідних даних\n");
            }

            pairs = pDescriptionStr.split(", ");

            if (!pDescriptionStr.equals("")) {
                for (String kv : pairs) {
                    String[] kvArr = kv.split("=");
                    pDescription.put(kvArr[0], kvArr[1]);
                }
            }

            shop.add(new Product(
                pName,
                pUnit,
                Integer.parseInt(pAmount),
                Double.parseDouble(pPrice),
                pReceiptDate,
                pDescription
            ));

            areaLogcat.appendText("Елемент додано успішно");
        } catch (Exception e) {
            areaLogcat.appendText("Помилка: " + e.toString() + "\n");
        }
    }

    public void onProductClearClick(ActionEvent actionEvent) {
        productNameInput.clear();
        productPriceInput.clear();
        productAmountInput.clear();
        productUnitInput.clear();
        productDescriptionInput.clear();
        productReceiptDateInput.setValue(null);
        areaLogcat.clear();
    }
}
