package ua.khpi.oop.zhukov04;
public class Helper {
    public static boolean DEBUG = false;
    public static String replaceByWordLength(String str, String replacement, int len) {
        String originalStr = str += " ";
        StringBuilder buf = new StringBuilder(originalStr);
        boolean match = true;
        int offset = 0;
        for (int i = 0, start = 0, end = 0; i < originalStr.length(); i++) {
            char ch = originalStr.charAt(i);
            if (DEBUG) System.out.printf("Char: [%d, %c]\n", i, ch);
            if (ch != ' ' && ch != '.' && ch != ',' && ch != '!' && ch != ':' && ch != ';') {
                if (match) {
                    start = i;
                    match = false;
                } else {
                    end = i; }
            } else {
                if (end - start == len - 1) {
                    end++;
                    if (replacement.length() > len) {
                        start += (replacement.length() - len) * offset;
                        end += (replacement.length() - len) * offset;
                    } else {
                        start -= (len - replacement.length()) * offset;
                        end -= (len - replacement.length()) * offset;
                    }
                    if (DEBUG) System.out.printf("Start&End: [%d,%d]\n", start, end);
                    buf.delete(start, end);
                    buf.insert(start, replacement);
                    if (DEBUG) System.out.printf("Buffer: %s\n", buf.toString());
                    offset++;
                    if (DEBUG) System.out.printf("Offset: %d\n",offset);
                    start = end = 0;
                }
                match = true;
            }
        }
        return buf.toString();
    }
}


