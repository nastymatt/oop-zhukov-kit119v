package ua.khpi.oop.zhukov16.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import ua.khpi.oop.zhukov07.Product;

import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.net.URL;
import java.util.ResourceBundle;

public class view_SearchElementController implements Initializable {
    @FXML
    public TextField searchKeyInput;
    @FXML
    public ListView searchResultsListView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public void onSearchButtonClick(MouseEvent event) {
        String searchQ = searchKeyInput.getText();
        ObservableList resultsList = FXCollections.observableArrayList(shop.find(product -> product.toString().contains(searchQ)));

        searchResultsListView.setItems(resultsList);
    }
}
