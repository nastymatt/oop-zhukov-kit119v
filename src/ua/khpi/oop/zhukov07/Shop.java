package ua.khpi.oop.zhukov07;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Клас "Магазин"
 *
 * Управління масивом об'єктів класу "Продукт"
 */
public class Shop {
    public ArrayList<Product> products; /* Масив об'єктів */

    public Shop() {
        products = new ArrayList<>();
    }

    /**
     * Додати продукт до масиву
     * @param product об'єкт який буде записано до масиву
     */
    public void add(Product product) {
        products.add(product);
    }

    /**
     * Видалити продукт з масиву
     * @param index індекс видаляємого об'єкта
     * @return продукт який було видалено
     */
    public Product remove(int index) {
        return products.remove(index);
    }

    /**
     * Отримати продукт з масиву
     * @param index індекс елемента
     * @return продукт який було знайдено
     */
    public Product get(int index) {
        return products.get(index);
    }

    /**
     * Ітератор для масиву продуктів
     * @return ітератор
     */
    public Iterator<Product> iterator() {
        return products.iterator();
    }

    /**
     * Вивід масиву продуктів в консоль
     */
    public void print() {
        for (int i = 0; i < products.size(); i++)
            System.out.printf("%d) %s\n", i + 1, products.get(i).toString());
    }

    /**
     * Розмір масиву продуктів
     * @return 0 якщо масив пустий
     */
    public int size() {
        return products.size();
    }


    /**
     * Зчитати масив продуктів з файлу в форматі XML
     * @param filePath шлях до файлу
     */
    public void read(String filePath) {
        try {
            XMLDecoder decoder = new XMLDecoder(new FileInputStream(filePath));
            products = new ArrayList<>((ArrayList<Product>) decoder.readObject());

            decoder.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Записати масив продуктів до файлу в форматі XML
     * @param filePath шлях до файлу
     *
     * Якщо файл не було знайдено, він буде створений
     */
    public void write(String filePath) {
        try {
            XMLEncoder encoder = new XMLEncoder(new FileOutputStream(filePath));

            encoder.writeObject(products);
            encoder.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
