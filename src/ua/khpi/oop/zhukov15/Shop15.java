package ua.khpi.oop.zhukov15;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov13.Util;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Клас контейнер для зберігання об'єктів прикладної області <code>Product</code>
 */
public class Shop15 extends ArrayList<Product> {
    /**
     * Пошук елементів конейтера
     * @param predicate правило за яким буде відбуватися пошук
     * @return <code>Product[]</code> масив знайдених елементів
     */
    public Object[] find(Predicate<Product> predicate) {
        return this.stream().filter(predicate).toArray();
    }

    /**
     * Заповнює контейнер об'єктами з випадковими значеннями
     * @param count кількість додаваємих елементів
     * @throws IllegalArgumentException якщо <code>count</code> менше або дорівнює 0
     */
    public void fill(int count) {
        if (count <= 0) {
            throw new IllegalArgumentException("Size can't be less or equals to 0");
        }

        for (int i = count - 1; i >= 0; i--) {
            this.add(Util.createRandomProduct());
        }
    }

    /**
     * Перетворює контейнер в масив
     * @return <code>Product[]</code> масив елементів
     */
    @Override
    public Product[] toArray() {
        return Arrays.copyOf(super.toArray(), size(), Product[].class);
    }

    /**
     * Записає дані контейнера у файл не використовуючи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void saveToFile(String filePath) throws IOException{
            Path path = Paths.get(filePath);
            StringBuilder stringBuilder = new StringBuilder();

            for (Product product : this) {
                stringBuilder
                        .append(product.toString())
                        .append('\n');
            }

            Files.writeString(path, stringBuilder.toString());
    }

    /**
     * Записує дані контейнера у файл використовуючи проток серіалізації
     * @param filePath шлях до файлу
     */
    public void saveToXMLFile(String filePath) throws IOException {
            XMLEncoder encoder = new XMLEncoder(new FileOutputStream(filePath));
            encoder.writeObject((Product[]) toArray());
            encoder.close();
    }

    /**
     * Відновлює дані контейнера з файлу не використовучи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void loadFromFile(String filePath) throws IOException{
            Stream<String> stream = Files.lines(Paths.get(filePath));

            this.clear();

            for (Iterator<String> it = stream.iterator(); it.hasNext();) {
                String line = it.next();

                add(new Product(line));
            }
    }

    /**
     * Відновлює дані контейнера з файлу використовучи протокол серіалізації
     * @param filePath шлях до файлу
     */
    public void loadFromXMLFile(String filePath) throws IOException {
            XMLDecoder decoder = new XMLDecoder(new FileInputStream(filePath));
            Product[] arr = (Product[]) decoder.readObject();

            this.clear();

            this.addAll(Arrays.asList(arr));

            decoder.close();
    }
}
