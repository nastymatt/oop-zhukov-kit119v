package ua.khpi.oop.zhukov08;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov07.Shop;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;

public class Main {
    /**
     * Очищення вікна консолі
     */
    public static void clearConsole() {
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }

    /**
     * Програмне меню
     */
    public static void appMenu() {
        Shop shop = new Shop();
        Scanner input = new Scanner(System.in);
        String filePath = "";
        int menuOption;
        File root;
        ArrayList<File> dir;

        while (true) {
            clearConsole();
            System.out.print(
                    "0. Вихід\n"
                    + "1. Вивести список товарів\n"
                    + "2. Додати товар\n"
                    + "3. Видалити товар\n"
                    + "4. Показати товар за номером в списку\n"
                    + "5. Зчитати список товарів з файлу\n"
                    + "6. Записати список товарів у файл\n"
                    + "--------------------------------------\n"
                    + "===> "
            );
            menuOption = input.nextInt();

            switch (menuOption) {
                case 0: return;
                case 1:
                    clearConsole();
                    shop.print();
                    input.nextLine();
                    input.nextLine();
                    break;
                case 2:
                    input.nextLine();
                    clearConsole();
                    System.out.print("Назва => ");
                    String pName = input.nextLine();
                    System.out.print("Одиниця вимірювання => ");
                    String pUnit = input.nextLine();
                    System.out.print("Кількість => ");
                    int pAmount = input.nextInt();
                    System.out.print("Ціна за одиницю => ");
                    double pPrice = input.nextDouble();
                    input.nextLine();
                    System.out.print("Дата прибуття => ");
                    String pReceiptDate = input.nextLine();
                    System.out.print("Опис товару => ");
                    String pDescriptionStr = input.nextLine();
                    Map<String, String> pDescription = new HashMap<>();

                    for (String kv : pDescriptionStr.split(",")) {
                        String[] kvArr = kv.split("=");
                        pDescription.put(kvArr[0], kvArr[1]);
                    }

                    shop.add(new Product(
                            pName,
                            pUnit,
                            pAmount,
                            pPrice,
                            pReceiptDate,
                            pDescription
                    ));
                    break;
                case 3:
                    clearConsole();
                    System.out.print("Індекс => ");
                    int rmIndex = input.nextInt();
                    if (rmIndex < 0 || rmIndex >= shop.size()) {
                        System.out.println("Неможливо знайти запис");
                    } else {
                        shop.remove(rmIndex);
                        System.out.println("Запис успішно видалено");
                    }
                    input.nextLine();
                    input.nextLine();
                    break;
                case 4:
                    clearConsole();
                    System.out.print("Індекс => ");
                    int gIndex = input.nextInt();
                    if (gIndex < 0 || gIndex >= shop.size()) {
                        System.out.println("Неможливо знайти запис");
                    } else {
                        System.out.println(shop.get(gIndex).toString());
                    }
                    input.nextLine();
                    input.nextLine();
                    break;
                case 5:
                    root = new File("/Users/nastymatt");
                    dir = listDirectoryFiles(root);
                    while (true) {
                        clearConsole();

                        System.out.print("0. Назад\n--------\n");
                        System.out.printf("Вміст директорії %s\n", root.getPath());
                        for (int i = 0; i < dir.size(); i++) {
                            System.out.printf("%d) %s\n", i + 1, dir.get(i).getName());
                        }
                        System.out.print("===> ");
                        menuOption = input.nextInt();

                        if (menuOption == 0) {
                            if (root.getPath().equals("/Users/nastymatt")) {
                                break;
                            } else {
                                root = root.getParentFile();
                                dir = listDirectoryFiles(root);
                                continue;
                            }
                        }

                        if (menuOption > 0 && menuOption <= dir.size()) {
                            File subFile = new File(root.getAbsolutePath(), dir.get(menuOption - 1).getName());

                            if (subFile.isDirectory()) {
                                root = subFile;
                                dir = listDirectoryFiles(root);
                            } else {
                                filePath = subFile.getAbsolutePath();
                                break;
                            }
                        }
                    }
                    shop.read(filePath);
                    break;
                case 6:
                    root = new File("/Users/nastymatt");
                    dir = listDirectoryFiles(root);
                    while (true) {
                        clearConsole();

                        System.out.print("0. Назад\n--------\n");
                        System.out.printf("Вміст директорії %s\n", root.getPath());
                        for (int i = 0; i < dir.size(); i++) {
                            System.out.printf("%d) %s\n", i + 1, dir.get(i).getName());
                        }
                        System.out.print("===> ");
                        menuOption = input.nextInt();

                        if (menuOption == 0) {
                            if (root.getPath().equals("/Users/nastymatt")) {
                                break;
                            } else {
                                root = root.getParentFile();
                                dir = listDirectoryFiles(root);
                                continue;
                            }
                        }

                        if (menuOption > 0 && menuOption <= dir.size()) {
                            File subFile = new File(root.getAbsolutePath(), dir.get(menuOption - 1).getName());

                            if (subFile.isDirectory()) {
                                root = subFile;
                                dir = listDirectoryFiles(root);
                            } else {
                                filePath = subFile.getAbsolutePath();
                                break;
                            }
                        }
                    }
                    shop.write(filePath);
                    break;
            }
        }
    }

    public static ArrayList<File> listDirectoryFiles(final File folder) {
        return new ArrayList<>(Arrays.asList(folder.listFiles()));
    }

    public static void main(String[] args) {
        clearConsole();
        appMenu();
    }
}
