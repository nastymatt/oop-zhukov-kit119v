package ua.khpi.oop.zhukov05;
import java.util.Iterator;
/**
 * Клас контейнер
 */
public class IList {
    /**
     * Масив даних для контейнера
     */
    private String[] arr;
    public IList() {
        arr = new String[]{};
    }
    /**
     * Повертає вміст контейнера у вигляді рядка
     * @return рядок даних контейра розділених пробілом
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (String s : arr)
            builder.append(s).append(" ");
        return builder.toString();
    }
    /**
     * Додає вказанний елемент до кінця контенеру
     * @param str рядок який буде додано
     */
    public void add(String str) {
        int i;
        String[] oldArr = arr;
        arr = new String[oldArr.length + 1];
        for (i = 0; i < oldArr.length; i++)
            arr[i] = oldArr[i];
        arr[i] = str;
    }

    /**
     * Видаляє всі елементи з контейнеру
     */
    public void clear() {
        arr = new String[]{};
    }
    /**
     * Видаляє перший випадок вказанного елемента з контейнера
     * @param str рядок для видалення
     * @return true якщо було знайдено випадок
     */
    public boolean remove(String str) {
        String[] oldArr = arr;
        if (!contains(str)) return false;
        arr = new String[oldArr.length - 1];
        for (int i = 0, j = 0; i < oldArr.length; i++)
            if (!oldArr[i].contains(str))
                arr[j++] = oldArr[i];
        return true;
    }
    /**
     * Повертає масив, що містить всі елементи  контейнері
     * @return масив елементів
     */
    public Object[] toArray() {
        return arr; }
    /**
     * Отримати розмір контейнеру
     * @return розмір контейнеру
     */
    public int size() {
        return arr.length;
    }
    /**
     * Знаходження вказанного елементу
     * @param str шуканий рядок
     * @return true якщо рядок було знайдено
     */
    public boolean contains(String str) {
        for (String s : arr)
            if (s.equals(str)) return true;

        return false;
    }
    /**
     * Порівняння контейнерів
     * @param list контейнер для порівняння
     * @return true якщо контейнер містить всі елементи з
    зазаначеного у параметрах
     */
    public boolean containsAll(IList list) {
        int i, j;
        for (i = 0; i < list.size(); i++) {
            for (j = 0; j < arr.length; j++)
                if (list.arr[i].equals(arr[j])) break;
            if (j == arr.length) return false;
        }
        return true;
    }
    /**
     * Класс ітератор
     * @return ітератор для контейнера елементів
     */
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private int currentIndex = 0;
            /**
             * Перевіряє існування наступного елемента контейнеру
             * @return true якщо елемент існує
             */
            @Override
            public boolean hasNext() {
                return currentIndex < size() && arr[currentIndex] != null;
            }
            /**
             * Повертає наступний елемент контейнеру
             * @return елемент контейнеру
             */
            @Override
            public String next() {
                return arr[currentIndex++];
            }
            /**
             * Видаляє поточний елемент с контейнера
             */

            @Override
            public void remove() {
                IList.this.remove(arr[currentIndex]);
            }
        }; }
}