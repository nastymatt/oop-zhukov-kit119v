package ua.khpi.oop.zhukov07;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Клас "Продукт"
 */
public class Product implements Serializable {
    private String name; /* Назва продукту */
    private String unit; /* Одиниця вимірювання */
    private int amount; /* Кількість */
    private double price; /* Ціна за одиницю */
    private String receiptDate; /* Дата прибуття */
    private Map<String, String> description; /* Опис у вигляді ключ=значення */

    public Product() {
        this.name = "";
        this.unit = "";
        this.amount = 0;
        this.price = 0;
        this.receiptDate = "";
        this.description = new HashMap<>();
    }

    public Product(String name, String unit, int amount, double price, String receiptDate, Map<String, String> description) {
        this.name = name;
        this.unit = unit;
        this.amount = amount;
        this.price = price;
        this.receiptDate = receiptDate;
        this.description = description;
    }

    public Product(String str) {
        this();

        Pattern p = Pattern.compile("(\\w|\\.|-)+,");
        Matcher m = p.matcher(str);
        String[] kv;
        String[] groups = new String[5];
        int i = 0;

        while (m.find() && i < 5) {
            groups[i] = m.group().replace(",", "");
            i++;
        }

        this.name = groups[0];
        this.unit = groups[1];
        this.amount = Integer.parseInt(groups[2]);
        this.price = Double.parseDouble(groups[3]);
        this.receiptDate = groups[4];

        str = str.replaceAll("(.+description=|\\{|})", "");
        System.out.println(str);
        kv = str.split(", ");

        if (kv.length > 0 && !kv[0].equals("")) {
            for (String pair : kv) {
                String[] sPair = pair.split("=");

                this.description.put(sPair[0], sPair[1]);
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public Map<String, String> getDescription() {
        return description;
    }

    public int getAmount() {
        return amount;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDescription(HashMap<String, String> description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name=" + name +
                ", unit=" + unit +
                ", amount=" + amount +
                ", price=" + price +
                ", receiptDate=" + receiptDate +
                ", description=" + description.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return amount == product.amount
                && Double.compare(product.price, price) == 0
                && name.equals(product.name)
                && unit.equals(product.unit)
                && receiptDate.equals(product.receiptDate)
                && description.equals(product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, unit, amount, price, receiptDate, description);
    }
}
