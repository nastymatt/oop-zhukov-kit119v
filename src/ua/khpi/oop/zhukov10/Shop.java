package ua.khpi.oop.zhukov10;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov09.NodeList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Прикладна область "Shop"
 */
public class Shop extends NodeList<Product> {
    /**
     * Сортування списку за вказаним полем
     * @param sortBy поле за яким буде відбуватись сортування
     * @param direction напрямок сортування, якщо <code>false</code> за зростанням, якщо <code>true</code> за спаданням
     */
    public void sort(ShopSort sortBy, boolean direction) {
        if (head == null) {
            return;
        }

        Product[] arr = toArray();
        Product temp;
        int i, j;

        switch (sortBy) {
            case BY_NAME:
                for (i = 0; i < arr.length; i++) {
                    for (j = i + 1; j < arr.length; j++) {
                        if (arr[i].getName().compareTo(arr[j].getName()) > 0) {
                            temp = arr[i];
                            arr[i] = arr[j];
                            arr[j] = temp;
                        }
                    }
                }
                break;
            case BY_DATE:
                for (i = 0; i < arr.length; i++) {
                    for (j = i + 1; j < arr.length; j++) {
                        try {
                            Date di = new SimpleDateFormat("dd-MM-yyyy").parse(arr[i].getReceiptDate());
                            Date dj = new SimpleDateFormat("dd-MM-yyyy").parse(arr[j].getReceiptDate());

                           if (di.compareTo(dj) > 0) {
                               temp = arr[i];
                               arr[i] = arr[j];
                               arr[j] = temp;
                           }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case BY_AMOUNT:
                for (i = 0; i < arr.length; i++) {
                for (j = i + 1; j < arr.length; j++) {
                    if (arr[i].getAmount() > arr[j].getAmount()) {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            break;
        }

        clear();

        if (direction) {
            for(i = 0; i < arr.length / 2; i++) {
                temp = arr[i];
                arr[i] = arr[arr.length - i - 1];
                arr[arr.length - i - 1] = temp;
            }
        }

        for (Product p : arr) {
            add(p);
        }
    }
}
