module oop.khpi {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.swing;
    requires javafx.media;
    requires javafx.web;

    opens ua.khpi.oop.zhukov16;
    opens ua.khpi.oop.zhukov16.Controllers;
    opens ua.khpi.oop.zhukov07;
}