package ua.khpi.oop.zhukov07;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Shop shop = new Shop();

        System.out.println("1) Додати новий елемент до масиву ->");
        shop.add(new Product("apple",
                "kg",
                20,
                35.3,
                "2018-10-16",
                new HashMap<>() {{
                    put("Країна", "Україна");
                    put("Колір", "Зелений");
                }}));
        shop.print();

        int index = 10;
        System.out.println("2) Отримати неіснуючий елемент масиву ->");
        if (index < 0 || index > shop.size()) {
            System.out.printf("Елемент за індексом %d не знайдено\n", index);
        }

        System.out.println("3) Видалити елемент з масиву ->");
        shop.remove(0);
        shop.print();
    }
}
