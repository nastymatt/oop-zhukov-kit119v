package ua.khpi.oop.zhukov16.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import ua.khpi.oop.zhukov07.Product;
import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.net.URL;
import java.util.ResourceBundle;


public class view_ShowContainerController implements Initializable {
    @FXML
    private ListView listView;
    @FXML
    private TextArea areaLogcat;
    @FXML
    private TextField elementsCountInput;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateListView();
    }

    private void updateListView() {
        listView.setItems(FXCollections.observableArrayList(shop));
    }

    public void onContainerClearButtonClick(MouseEvent event) {
        shop.clear();
        updateListView();
        areaLogcat.appendText("Успішно очищено\n");
    }

    public void onContainerFillButtonClick(MouseEvent event) {
        try {
            shop.fill(Integer.parseInt(elementsCountInput.getText()));
            updateListView();
            elementsCountInput.clear();
            areaLogcat.appendText("Успішно заповнено\n");
        } catch (Exception e) {
            areaLogcat.appendText("Помилка: " + e.toString() + "\n");
        }
    }
}
