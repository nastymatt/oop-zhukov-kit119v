package ua.khpi.oop.zhukov12;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov09.NodeList;
import ua.khpi.oop.zhukov11.Shop11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Прикладна область "Магазин"
 */
public class Shop12 extends Shop11 {
    /**
     * Отримати список товарів з актуальним терміном придатності
     * @return <code>NodeList</code> список продуктів які пройшли перевірку
     */
    public NodeList<Product> findByActualExpirationDate() {
        NodeList<Product> list = new NodeList<>();
        Map<String, String> description;
        Date currentDate = new Date();
        Date expirationDate;

        for (Product p : this) {
            description = p.getDescription();

            if (description.containsKey("expirationDate")) {
                String strDate = description.get("expirationDate");

                if (!strDate.matches("^\\d{2}[-.]\\d{2}[-.]\\d{4}$")) {
                    continue;
                }

                try {
                    expirationDate = new SimpleDateFormat("dd-MM-yyyy").parse(strDate);

                    if (expirationDate.compareTo(currentDate) > 0) {
                        list.add(p);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (description.containsKey("storageTime")) {
                String strTime = description.get("storageTime");

                try {
                    expirationDate = new SimpleDateFormat("dd-MM-yyyy").parse(p.getReceiptDate());

                    if (!strTime.matches("^\\d+$")) {
                        continue;
                    }

                    int days = Integer.parseInt(strTime);

                    expirationDate = DateUtil.addDays(expirationDate, days);

                    if (expirationDate.compareTo(currentDate) > 0) {
                        list.add(p);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        return list;
    }
}
