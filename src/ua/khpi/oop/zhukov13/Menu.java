package ua.khpi.oop.zhukov13;

import ua.khpi.oop.zhukov09.NodeList;

import java.util.HashMap;

/**
 * Клас меню
 * Створює консольне меню для управління програмою
 */
public class Menu {
    private int currentOption;
    private final HashMap<Integer, String> options;
    private final NodeList<onMenuOption> listeners;
    private boolean optionsMade;

    public Menu() {
        currentOption = 0;
        options = new HashMap<>();
        listeners = new NodeList<>();
        optionsMade = false;
    }

    /**
     * Створює новий пункт меню та прив'язую до нього переданий обробник
     * @param label текст який буде відображено в пункті меню
     * @param listener обробник вибору пункта меню
     * @return Menu поточний об'єкт
     */
    public Menu on(String label, onMenuOption listener) {
        int option = options.size();

        if (options.containsKey(option)) {
            return this;
        }

        options.put(options.size(), label);
        listeners.add(listener);

        return this;
    }

    /**
     * Очищує вікно консолі, виводить створене меню
     */
    public void launch() {
        int key;

        if (!optionsMade) {
            makeMenuOptions();
        }

        while (true) {
            Util.clearConsole();

            for (int i = 0; i < options.size(); i++) {
                if (i == currentOption) {
                    System.out.println(options.get(i).replace(" ]", "x]"));
                    continue;
                }

                System.out.println(options.get(i));
            }

            key = Util.getCh();

            if (key == 27) {
                break;
            }

            switch (key) {
                case 38:
                    if (currentOption != 0) {
                        currentOption--;
                    }
                    break;
                case 40:
                    if (currentOption < options.size() - 1) {
                        currentOption++;
                    }
                    break;
                case 10:
                    listeners.getValue(currentOption).onMenuOptionListener();
                    break;
            }
        }
    }

    /**
     * Перетворює текст в пункт меню
     */
    private void makeMenuOptions() {
        int maxLen = options.values()
                .stream()
                .mapToInt(String::length)
                .max()
                .orElse(0);

        options.forEach((k, v) -> {
            v += " ".repeat(maxLen - v.length() + 4) + "[ ]";
            options.put(k, v);
        });

        optionsMade = true;
    }
}
