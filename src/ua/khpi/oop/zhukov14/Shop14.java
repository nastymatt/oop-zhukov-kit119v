package ua.khpi.oop.zhukov14;

import ua.khpi.oop.zhukov07.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Shop14 extends ua.khpi.oop.zhukov13.Shop13 {
    /**
     * Підраховує середню кількість серед товарів
     */
    public void averageAmount() {
        if (getSize() == 0) {
            return;
        }

        int avgAmount = 0;

        try {
            for (Product p : this) {
                avgAmount += p.getAmount();

                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        avgAmount /= getSize();
    }

    /**
     * Підраховує максимальну ціну серед товарів
     */
    public void maxPrice() {
        if (getSize() == 0) {
            return;
        }

        double _maxPrice = getValue(0).getPrice();

        try {
            for (int i = 0; i < getSize(); i++) {
                for (int j = i + 1; j < getSize(); j++) {
                    if (getValue(i).getPrice() > _maxPrice) {
                        _maxPrice = getValue(i).getPrice();
                    }
                }
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Знаходить мінімальну дату серед товарів
     */
    public void minDate() {
        if (getSize() == 0) {
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        String _minDate = getValue(0).getReceiptDate();

        try {
            for (int i = 0; i < getSize(); i++) {
                for (int j = i + 1; j < getSize(); j++) {
                    Date di = dateFormat.parse(getValue(i).getReceiptDate());
                    Date dj = dateFormat.parse(getValue(j).getReceiptDate());

                    if (di.compareTo(dj) < 0) {
                        _minDate = getValue(i).getReceiptDate();
                    }
                }
                Thread.sleep(10);
            }
        } catch (ParseException|InterruptedException e) {
            e.printStackTrace();
        }
    }
}
