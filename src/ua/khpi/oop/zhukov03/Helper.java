package ua.khpi.oop.zhukov03;

/**
 * Helper Class
 */
public class Helper {
    /**
     * Заміняє в тексті слова заданної довжини
     * @param str текст
     * @param replacement слово для заміни
     * @param len довжина слів для заміни
     * @return текст з замінами
     */
    public static String replaceByWordLength(String str, String
            replacement, int len) {
        String originalStr = str += " ";
        StringBuilder buf = new StringBuilder(originalStr);
        boolean match = true;
        int offset = 0;
        for (int i = 0, start = 0, end = 0; i < originalStr.length(); i++) {
            char ch = originalStr.charAt(i);
            if (ch != ' ' && ch != '.' && ch != ',' && ch != '!' && ch != ':' && ch != ';') {
                if (match) {
                    start = i;
                    match = false;
                } else {
                    end = i; }
            } else {
                if (end - start == len - 1) {
                    end++;
                    if (replacement.length() > len) {
                        start += (replacement.length() - len) * offset;
                        end += (replacement.length() - len) * offset;
                    } else {
                        start -= (len - replacement.length()) * offset;
                        end -= (len - replacement.length()) * offset;
                    }
                    buf.delete(start, end);
                    buf.insert(start, replacement);
                    offset++;
                    start = end = 0;
                }
                match = true;
            }
        }
        System.out.printf("[Length = %d, Replacement = %s]\n", len, replacement);
        System.out.printf("\u001B[31m -> Original Text:\u001B[0m %s\n", str);
        System.out.printf("\u001B[32m -> Result Text:\u001B[0m %s\n", buf.toString());
        return buf.toString();
    }
}