package ua.khpi.oop.zhukov14;

import ua.khpi.oop.zhukov13.Menu;
import ua.khpi.oop.zhukov13.Util;
import ua.khpi.oop.zhukov09.NodeList;
import ua.khpi.oop.zhukov10.ShopSort;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    static Shop14 shop = new Shop14();

    public static void main(String[] args) {
        NodeList<String> argsList = NodeList.fromArray(args);

        if (argsList.contains("-auto")) {
            modeAuto();
            return;
        }

        modeMenu();
    }

    public static void modeAuto() {
        final int randomsCount = 500;

        shop.fillRandom(randomsCount);
        System.out.printf("Fill list with objects with random values: %s\n\n", randomsCount);

       printTable();
    }

    public static void modeMenu() {
        Menu mainMenu = new Menu();
        Menu readFileMenu = new Menu();
        Menu writeFileMenu = new Menu();
        Menu sortMenu = new Menu();
        Scanner scanner = new Scanner(System.in);

        sortMenu
                .on("+ By Name", () -> shop.sort(ShopSort.BY_NAME, false))
                .on("+ By Amount", () -> shop.sort(ShopSort.BY_AMOUNT, false))
                .on("+ By Date", () -> shop.sort(ShopSort.BY_DATE, false))
                .on("- By Name", () -> shop.sort(ShopSort.BY_NAME, true))
                .on("- By Amount", () -> shop.sort(ShopSort.BY_AMOUNT, true))
                .on("- By Date", () -> shop.sort(ShopSort.BY_DATE, true));

        writeFileMenu
                .on("Write as Text", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.write(filePath);
                        System.out.println("Write file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Write as XML", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.writeXML(filePath);
                        System.out.println("Write file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                });

        readFileMenu
                .on("Read as Text", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.read(filePath);
                        System.out.println("Read file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Read as XML", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.readXML(filePath);
                        System.out.println("Read file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                });

        mainMenu
                .on("Print", () -> {
                    System.out.println(shop.toString());
                    scanner.nextLine();
                })
                .on("Read from file", readFileMenu::launch)
                .on("Write to file", writeFileMenu::launch)
                .on("Fill with random values", () -> {
                    System.out.print("Number of elements: ");
                    String countStr = scanner.nextLine();

                    try {
                        shop.fillRandom(Integer.parseInt(countStr));
                        System.out.println("Fill with random values successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Input Element", () -> {
                    try {
                        shop.add(ua.khpi.oop.zhukov12.Main.inputProduct(scanner));
                        System.out.println("Element added successfully");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Remove element", () -> {
                    System.out.print("Index to remove: ");
                    String rmIndexStr = scanner.nextLine();

                    try {
                        shop.remove(Integer.parseInt(rmIndexStr));
                        System.out.println("Element deleted successfully");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Contains element", () -> {
                    try {
                        if (shop.contains(ua.khpi.oop.zhukov12.Main.inputProduct(scanner))) {
                            System.out.println("True");
                            return;
                        }

                        System.out.println("False");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Thread tests", () -> {
                    Util.clearConsole();

                    printTable();

                    scanner.nextLine();
                })
                .on("Sort", sortMenu::launch)
                .on("Clear", () -> {
                    shop.clear();
                    System.out.println("Container has been cleared");
                    scanner.nextLine();
                })
                .on("Exit", () -> System.exit(0));

        mainMenu.launch();
    }

    public static void printTable() {
        Thread th1 = new Thread(shop::minDate);
        Thread th2 = new Thread(shop::maxPrice);
        Thread th3 = new Thread(shop::averageAmount);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        long start1ms, start2ms,
                fin1ms, fin2ms;
        String start1Time, start2Time,
                fin1Time, fin2Time;
        String leftAlignFormat = "| %-15s | %-14s | %-15s | %-17s |%n";

        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
        System.out.format("| Record          | Start time     | Finish time     | Execution time    |%n");
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");

        start1Time = dateFormat.format(Calendar.getInstance().getTime());
        start1ms = System.currentTimeMillis();
        th1.start();
        th2.start();
        th3.start();
        try {
            th1.join();
            th2.join();
            th3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fin1ms = System.currentTimeMillis();
        fin1Time = dateFormat.format(Calendar.getInstance().getTime());

        start2Time = dateFormat.format(Calendar.getInstance().getTime());
        start2ms = System.currentTimeMillis();
        shop.minDate();
        shop.maxPrice();
        shop.averageAmount();
        fin2ms = System.currentTimeMillis();
        fin2Time = dateFormat.format(Calendar.getInstance().getTime());

        System.out.printf(leftAlignFormat, "Parallel", start1Time, fin1Time, (fin1ms - start1ms) + "ms");
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
        System.out.printf(leftAlignFormat, "Consistently", start2Time, fin2Time, (fin2ms - start2ms) + "ms");
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
        System.out.format("| Difference      | in %.2f        |                 |                   |%n",
                (double) (fin2ms - start2ms) / (double) (fin1ms - start1ms));
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
        System.out.format("| Elements        | %-14d |                 |                   |%n",
                shop.getSize());
        System.out.format("+-----------------+----------------+-----------------+-------------------+%n");
    }
}

