package ua.khpi.oop.zhukov12;

import java.util.Calendar;
import java.util.Date;

/**
 * Helper клас
 * Містить методи для роботи з датами
 */
public class DateUtil {
    /**
     * Додати дні до вказаної дати
     * @param date дата до якої буде викононо додавання днів
     * @param days кількість додаваємих днів
     * @return Date - дата з доданими до неї днями
     */
    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }
}
