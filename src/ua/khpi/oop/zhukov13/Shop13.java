package ua.khpi.oop.zhukov13;

public class Shop13 extends ua.khpi.oop.zhukov12.Shop12 {
    /**
     * Заповнює контейнер об'єктами Product з випадковими даними
     * @param count <code>int</code> кількість елементів які будуть створені
     */
    public void fillRandom(int count) {
        for (int i = count; i > 0; i--) {
            add(Util.createRandomProduct());
        }
    }
}
