package ua.khpi.oop.zhukov13;

import ua.khpi.oop.zhukov07.Product;
import ua.khpi.oop.zhukov09.NodeList;
import ua.khpi.oop.zhukov10.ShopSort;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    static Shop13 shop = new Shop13();

    public static void main(String[] args) {
        NodeList<String> argsList = NodeList.fromArray(args);

        if (argsList.contains("-auto")) {
            modeAuto();
            return;
        }

        modeMenu();
    }

    public static void modeAuto() {
        final long time = 5000;
        final int randomsCount = 5000;

        shop.fillRandom(randomsCount);
        System.out.println("Fill list with objects with random values: " + randomsCount);

        minDateThread(time);
        maxPriceThread(time);
        averageAmountThread(time);
    }

    public static void modeMenu() {
        Menu mainMenu = new Menu();
        Menu readFileMenu = new Menu();
        Menu writeFileMenu = new Menu();
        Menu sortMenu = new Menu();
        Scanner scanner = new Scanner(System.in);

        sortMenu
                .on("+ By Name", () -> shop.sort(ShopSort.BY_NAME, false))
                .on("+ By Amount", () -> shop.sort(ShopSort.BY_AMOUNT, false))
                .on("+ By Date", () -> shop.sort(ShopSort.BY_DATE, false))
                .on("- By Name", () -> shop.sort(ShopSort.BY_NAME, true))
                .on("- By Amount", () -> shop.sort(ShopSort.BY_AMOUNT, true))
                .on("- By Date", () -> shop.sort(ShopSort.BY_DATE, true));

        writeFileMenu
                .on("Write as Text", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.write(filePath);
                        System.out.println("Write file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Write as XML", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.writeXML(filePath);
                        System.out.println("Write file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                });

        readFileMenu
                .on("Read as Text", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.read(filePath);
                        System.out.println("Read file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Read as XML", () -> {
                    String filePath;
                    System.out.print("File path: ");
                    filePath = scanner.nextLine();

                    try {
                        shop.readXML(filePath);
                        System.out.println("Read file successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                });

        mainMenu
                .on("Print", () -> {
                    System.out.println(shop.toString());
                    scanner.nextLine();
                })
                .on("Read from file", readFileMenu::launch)
                .on("Write to file", writeFileMenu::launch)
                .on("Fill with random values", () -> {
                    System.out.print("Number of elements: ");
                    String countStr = scanner.nextLine();

                    try {
                        shop.fillRandom(Integer.parseInt(countStr));
                        System.out.println("Fill with random values successful");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Input Element", () -> {
                    try {
                        shop.add(ua.khpi.oop.zhukov12.Main.inputProduct(scanner));
                        System.out.println("Element added successfully");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Remove element", () -> {
                    System.out.print("Index to remove: ");
                    String rmIndexStr = scanner.nextLine();

                    try {
                        shop.remove(Integer.parseInt(rmIndexStr));
                        System.out.println("Element deleted successfully");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    scanner.nextLine();
                })
                .on("Contains element", () -> {
                  try {
                      if (shop.contains(ua.khpi.oop.zhukov12.Main.inputProduct(scanner))) {
                          System.out.println("True");
                          return;
                      }

                      System.out.println("False");
                  } catch (Exception e) {
                      e.printStackTrace();
                  }
                  scanner.nextLine();
                })
                .on("Thread tests", () -> {
                    Util.clearConsole();

                    System.out.print("Thread timeout [in ms]: ");
                    String timeoutStr = scanner.nextLine();
                    long time = -1;

                    if (timeoutStr.equals("")) {
                        time = 0;
                    }

                    try {
                        if (time == -1) {
                            time = Long.parseLong(timeoutStr);
                        }

                        minDateThread(time);
                        maxPriceThread(time);
                        averageAmountThread(time);
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                    scanner.nextLine();
                })
                .on("Sort", sortMenu::launch)
                .on("Clear", () -> {
                    shop.clear();
                    System.out.println("Container has been cleared");
                    scanner.nextLine();
                })
                .on("Exit", () -> System.exit(0));

        mainMenu.launch();
    }

    public static void minDateThread(final long time) {
        new Thread(() -> {
            long maxTime = System.currentTimeMillis() + time;

            System.out.println("Min date thread start!");

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
            String minDate = "";

            try {
                for (int i = 0; i < shop.getSize(); i++) {
                    if (time != 0 && maxTime < System.currentTimeMillis()) {
                        System.out.println("Min date thread time over!");
                        return;
                    }

                    for (int j = i + 1; j < shop.getSize(); j++) {
                        Date di = dateFormat.parse(shop.getValue(i).getReceiptDate());
                        Date dj = dateFormat.parse(shop.getValue(j).getReceiptDate());

                        if (di.compareTo(dj) < 0) {
                            minDate = shop.getValue(i).getReceiptDate();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("Min date is " + minDate);
        }).start();
    }

    public static void maxPriceThread(final long time) {
        new Thread(() -> {
            long maxTime = System.currentTimeMillis() + time;

            System.out.println("Max price thread start!");

            double maxPrice = 0;

            for (int i = 0; i < shop.getSize(); i++) {
                if (time != 0 && maxTime < System.currentTimeMillis()) {
                    System.out.println("Max price thread time over!");
                    return;
                }

                for (int j = i + 1; j < shop.getSize(); j++) {
                    if (shop.getValue(i).getPrice() > maxPrice) {
                        maxPrice = shop.getValue(i).getPrice();
                    }
                }
            }

            System.out.println("Max price is: " + maxPrice);
        }).start();
    }

    public static void averageAmountThread(final long time) {
        new Thread(() -> {
            long maxTime = System.currentTimeMillis() + time;

            System.out.println("Average amount thread start!");

            long avgAmount = 0;

            for (Product p : shop) {
                if (time != 0 && maxTime < System.currentTimeMillis()) {
                    System.out.println("Average amount time over!");
                    return;
                }

                avgAmount += p.getAmount();
            }

            System.out.println("Average amount is: " + avgAmount / shop.getSize());
        }).start();
    }
}

