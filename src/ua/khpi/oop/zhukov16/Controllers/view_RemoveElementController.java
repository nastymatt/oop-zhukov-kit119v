package ua.khpi.oop.zhukov16.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import static ua.khpi.oop.zhukov16.GlobalShop.shop;

import java.net.URL;
import java.util.ResourceBundle;

public class view_RemoveElementController implements Initializable {
    @FXML
    public TextArea areaLogcat;
    @FXML
    public TextField removeIndexInput;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void onRemoveElementButtonClick(MouseEvent event) {
        try {
            shop.remove(Integer.parseInt(removeIndexInput.getText()));
            areaLogcat.appendText("Елемент успішно видалено");
        } catch (Exception e) {
            areaLogcat.appendText("Помилка: " + e.toString() + "\n");
        }
    }
}
